import sequelize.*;
import sequelize.model.ModelName;
import sequelize.model.ModelSchema;

public class Main {
    public static void main(String[] args){
        Sequelize seq = new Sequelize("postgres","rsadmin","rsadminpassword");

        seq.define(
                new ModelName("User"),
                new ModelSchema()
                        .insert(new ModelName(""))
        );
    }
}
