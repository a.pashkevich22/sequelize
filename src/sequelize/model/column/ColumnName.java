package sequelize.model.column;

public class ColumnName {
    private String name;
    public ColumnName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
