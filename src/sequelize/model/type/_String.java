package sequelize.model.type;

public class _String extends ModelTypeParent {
    @Override
    public Boolean is(Object obj){
        return true;
    }

    @Override
    public String typeName() {
        return "TEXT";
    }
}
