package sequelize.model;

import sequelize.model.type.ModelTypeParent;

import java.util.HashMap;
import java.util.Map;

public class ModelSchema {
    Map<ModelName, ModelTypeParent> schema = new HashMap<>();
    public ModelSchema ModelSchema(){
        return this;
    }

    public ModelSchema insert(ModelName name, ModelTypeParent type){
        schema.put(name, type);
        return this;
    }
}
