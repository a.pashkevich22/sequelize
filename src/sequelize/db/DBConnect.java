package sequelize.db;

import java.sql.*;

public class DBConnect {
    public DBConnect DBConnect() throws SQLException {
        return this;
    }

    public Connection connect(String url, String username, String password) throws SQLException{
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, username, password);
    }
}
