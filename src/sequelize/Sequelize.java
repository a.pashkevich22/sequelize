package sequelize;

import sequelize.db.DBConnect;
import sequelize.model.ModelName;
import sequelize.model.ModelSchema;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Sequelize {
    Connection connection;
    Map<ModelName, ModelSchema> models = new HashMap<>();
    public Sequelize(String database, String username, String password){
        String url = "jdbc:postgresql://localhost/"+database;
        try {
            this.connection = new DBConnect().connect(url,username,password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void define(ModelName name, ModelSchema schema){
        models.put(name, schema);
    }
}
